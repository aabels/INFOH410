package com.nullprogram.maze;
import java.util.*;
public class Measurement {
    public static final Map<Integer,String> prefixes;
    static {
        Map<Integer,String> tempPrefixes = new HashMap<Integer,String>();
        tempPrefixes.put(0,"");
        tempPrefixes.put(3,"k");
        tempPrefixes.put(6,"M");
        tempPrefixes.put(9,"G");
        tempPrefixes.put(12,"T");
        tempPrefixes.put(-3,"m");
        tempPrefixes.put(-6,"μ");
        prefixes = Collections.unmodifiableMap(tempPrefixes);
    }

    String type;
    double value;

    public Measurement(double value, String type) {
        this.value = value;
        this.type = type;
    }

    public String toString() {
        double tval = value;
        int order = 0;
        if(tval != 0) {
	        while(Math.abs(tval) > 1000.0) {
	            tval /= 1000.0;
	            order += 3;
	        }
	        while(Math.abs(tval) < 1.0) {
	            tval *= 1000.0;
	            order -= 3;
	        }
        }
        String val = tval+"";
        int substringindex = Math.min(4,val.indexOf(".")+2);
        String sub = val.substring(0,substringindex);
        if(sub.endsWith(".")) {
        	sub = sub.substring(0, sub.length()-1);
        }
        return sub + prefixes.get(order) + type;
    }


}