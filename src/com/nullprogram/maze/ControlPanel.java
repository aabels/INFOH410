package com.nullprogram.maze;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControlPanel extends JPanel {

	boolean pause = false;
	MazeSolver solver;
	JSeparator sep = new JSeparator();
	JTextField txtAlphaValue = new JTextField(10);
	JLabel lblAlpha = new JLabel("Learning rate");
	JTextField txtRValue = new JTextField(10);
	JLabel lblR = new JLabel("Default reward:");
	JTextField txtDiscount = new JTextField(10);
	JLabel lblDiscount = new JLabel("Discount factor:");
	JLabel lblendR = new JLabel("End reward:");
	JTextField txtEndRValue = new JTextField(10);
	JLabel lblExplorationFactor = new JLabel("Exploration factor:");
	JTextField txtExplorationFactor = new JTextField(10);
	JSlider sldComplexity = new JSlider();
	JLabel complexity = new JLabel("Complexity:");
	JSlider sldCellSize = new JSlider();
	JLabel cellSize = new JLabel("Cell Size:");
	JButton restart = new JButton("Restart");
	JButton generate = new JButton("Regenerate");
	JButton stop = new JButton("Pause");
	JLabel lblAntCount = new JLabel("Ant count:");
	JTextField txtAntCount = new JTextField(10);
	JCheckBox ckbMethod = new JCheckBox("Ant Colony");

	JLabel lblAntAlpha = new JLabel("Pheromone influence:");
	JTextField txtAntAlpha = new JTextField(10);

	JLabel lblAntBeta = new JLabel("Attractiveness influence:");
	JTextField txtAntBeta = new JTextField(10);

	JLabel lblAntRho = new JLabel(" pheromone evaporation coefficient:");
	JTextField txtAntRho = new JTextField(10);

	JLabel lblAntQ = new JLabel(" Constant Q");
	JTextField txtAntQ = new JTextField(10);
	JLabel lblSleep = new JLabel("Sleep timer:");
	JSlider sldSleep = new JSlider();
	Maze maze;
	MazeDisplay display;
	int mazeWidth;
	int mazeHeight;
	int mazeScale;

	/**
	 * Create the panel.
	 */
	public ControlPanel(MazeDisplay display, int mazeWidth, int mazeHeight, int mazeScale) {
		this.mazeHeight = mazeHeight;
		this.mazeScale = mazeScale;
		this.mazeWidth = mazeWidth;
		this.display = display;
		setLayout(new GridLayout(20, 2, 0, 0));

		add(lblAlpha);
		txtAlphaValue.setText("1");

		add(txtAlphaValue);

		add(lblDiscount);
		txtDiscount.setText("0.99");
		add(txtDiscount);

		add(lblR);
		txtRValue.setText("-.1");
		add(txtRValue);

		add(lblendR);
		txtEndRValue.setText("10");
		add(txtEndRValue);


		add(lblExplorationFactor);
		txtExplorationFactor.setText("2");
		add(txtExplorationFactor);

		add(complexity);
		sldComplexity.setValue(95);
		sldComplexity.setMaximum(100);
		sldComplexity.setMinimum(90);
		add(sldComplexity);

		add(cellSize);
		sldCellSize.setValue(20);
		sldCellSize.setMaximum(100);
		sldCellSize.setMinimum(15);
		add(sldCellSize);

		add(lblAntCount);
		txtAntCount.setText("100");
		add(txtAntCount);
		add(lblAntAlpha);
		txtAntAlpha.setText("1");
		add(txtAntAlpha);
		add(lblAntBeta);
		txtAntBeta.setText("1");
		add(txtAntBeta);

		add(lblAntRho);
		txtAntRho.setText("0.02");
		add(txtAntRho);
		add(lblAntQ);
		txtAntQ.setText("100");
		add(txtAntQ);

		add(ckbMethod);
		add(restart);
		add(generate);
		add(stop);

		sldSleep.setMinimum(0);
		sldSleep.setMaximum(100);
		sldSleep.setValue(0);
		add(lblSleep);
		add(sldSleep);
		generateMaze();
		toggleMethod();
		ckbMethod.addActionListener((new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				toggleMethod();
			}
		}));
		restart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pause = false;
				stop.setText("Pause");
				solve();
			}
		});

		generate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generateMaze();
			}
		});

		stop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				toggleStop();
			}
		});
		sldSleep.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				if(solver != null) {
					solver.setSleepTime(getSleepValue());
				}
			}
		});
	
	}

	public void toggleStop() {
		pause = !pause;
		if(pause) {
			solver.pause();
			stop.setText("Play");
		} else {
			solver.play();
			stop.setText("Pause");
		}
	}
	
	public Double getExplorationFactor() {
		return Double.parseDouble(txtExplorationFactor.getText());
	}

	public void toggleMethod() {
		txtAlphaValue.setEnabled(!ckbMethod.isSelected());
		txtDiscount.setEnabled(!ckbMethod.isSelected());
		txtRValue.setEnabled(!ckbMethod.isSelected());
		txtEndRValue.setEnabled(!ckbMethod.isSelected());
		txtExplorationFactor.setEnabled(!ckbMethod.isSelected());
		txtAntCount.setEnabled(ckbMethod.isSelected());
		txtAntAlpha.setEnabled(ckbMethod.isSelected());
		txtAntBeta.setEnabled(ckbMethod.isSelected());
		txtAntRho.setEnabled(ckbMethod.isSelected());
		txtAntQ.setEnabled(ckbMethod.isSelected());
		
		if (solver != null) {
			solver.stop();
		}
		((DepthMaze) maze).convert(ckbMethod.isSelected() ? MazeSolver.METHOD_ANT : MazeSolver.METHOD_Q);
	}

	public int getAntCount() {
		return Integer.parseInt(txtAntCount.getText());
	}

	public Double getAntAlphaValue() {
		return Double.parseDouble(txtAntAlpha.getText());
	}

	public Double getAntBetaValue() {
		return Double.parseDouble(txtAntBeta.getText());
	}

	public double getAntRhoValue() {
		return Double.parseDouble(txtAntRho.getText());
	}

	public double getAntQValue() {
		return Double.parseDouble(txtAntQ.getText());
	}

	public int getComplexity() {
		return sldComplexity.getValue();
	}

	public Double getDefaultReward() {
		return Double.parseDouble(txtRValue.getText());
	}

	public Double getEndReward() {
		return Double.parseDouble(txtEndRValue.getText());
	}

	public int getCellSize() {
		return sldCellSize.getValue();
	}

	public double getDiscountFactor() {
		return Double.parseDouble(txtDiscount.getText());
	}

	public double getLearningRate() {
		return Double.parseDouble(txtAlphaValue.getText());
	}

	public void generateMaze() {
		maze = new DepthMaze(mazeWidth / getCellSize(), mazeHeight / getCellSize(), getCellSize(), getComplexity(),
				getEndReward(), getDefaultReward(),
				ckbMethod.isSelected() ? MazeSolver.METHOD_ANT : MazeSolver.METHOD_Q);
		display.setEndReward(getEndReward());

		if(!ckbMethod.isSelected()) {
			((DepthMaze) maze).applyExplorationFactor(getExplorationFactor());
		}
		display.setMaze(maze);
	}

	public int getSleepValue() {
	
		double sleep = sldSleep.getValue()/10;
		if(sleep == 0) {
			return 0;
		}
		return (int)(Math.pow(sleep,3));
	}

	public void solve() {
		if(solver!=null) {
			solver.stop();
		}
		if (!ckbMethod.isSelected()) {
			((DepthMaze) maze).applyDefaultR(getDefaultReward());
			((DepthMaze) maze).applyEndR(getEndReward());
			((DepthMaze) maze).applyExplorationFactor(getExplorationFactor());
			display.setEndReward(getEndReward());
		}
		solver = new MazeSolver(maze, getSleepValue());
		solver.setAntCount(getAntCount());
		solver.setAntAlpha(getAntAlphaValue());
		solver.setAntBeta(getAntBetaValue());
		MazeSolver.setAntRho(getAntRhoValue());
		MazeSolver.setAntQ(getAntQValue());
		solver.setDiscountFactor(getDiscountFactor());
		solver.setLearningRate(getLearningRate());
		solver.addListener(display);
		solver.start(ckbMethod.isSelected() ? MazeSolver.METHOD_ANT : MazeSolver.METHOD_Q);
	}

}
