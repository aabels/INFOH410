package com.nullprogram.maze;

import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.List;

public class AntCell extends DepthCell {
	private int antCount = 0;

	private List<AntAction> actions = new ArrayList<AntAction>();

	public List<AntAction> getActions() {
		return actions;
	}

	public void setActions(List<AntAction> actions) {
		this.actions = actions;
	}

	private double attractiveness = 0;

	public double getAttractiveness() {
		return attractiveness;
	}

	public void setAttractiveness(double attractiveness) {
		this.attractiveness = attractiveness;
	}

	public AntCell(int posX, int posY, int scale) {
		super(posX, posY, scale);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Add a connecting cell to this cell.
	 * 
	 * @param cell
	 *            a new cell connection
	 */
	@Override
	public void addNeighbor(final DepthCell cell) {
		AntAction action = new AntAction(cell, this);
		actions.add(action);
		if (cell.x < x) {
			left = false;
		} else if (cell.y < y) {
			top = false;
		}

		if (!neighbors.contains(cell)) {
			neighbors.add(cell);
		}
	}

	@Override
	public AntAction getOptimalAction() {
		AntAction optimal = null;
		for (AntAction action : actions) {
			if (optimal == null || action.getPherTrail() > optimal.getPherTrail()) {
				optimal = action;
			}
		}
		return optimal;
	}

	@Override
	public void removeNeighbor(DepthCell cell) {
		if(this.neighbors.contains(cell)) {
			this.neighbors.remove(cell);
			if (cell.x < x) {
				left = true;
			} else if (cell.y < y) {
				top = true;
			}

			List<Action> toRemove = new ArrayList<Action>();
			for (Action action : actions) {
				if (action.destination == cell) {
					toRemove.add(action);
				}
			}
			actions.removeAll(toRemove);

		}

	}

	public double getMaxPher() {
		AntAction optimal = getOptimalAction();
		return optimal != null ? optimal.getPherTrail() : 0;
	}

	

	public void addAnt() {
		antCount++;
	}

	public void removeAnt() {
		antCount--;
	}

	public int getAntCount() {
		return antCount;
	}

	public void setAntCount(int i) {
		this.antCount = 0;

	}
}
