package com.nullprogram.maze;

import java.util.LinkedList;
import java.util.List;

public class Ant {
	/**
	 * La distance parcourue par la fourmi depuis le d�part
	 */
	private int distance = 0;

	/**
	 * Cellule sur laquelle se trouve la fourmi
	 */
	AntCell activeCell;

	/**
	 * Derniere action effectu�e par la fourmi
	 */
	private AntAction lastAction = null;

	/**
	 * Liste de toutes les actions effectu�es depuis le d�part
	 */
	private LinkedList<AntAction> actionHistory = new LinkedList<AntAction>();

	/**
	 * La fourmi est-elle sur le retour apr�s avoir trouv� la fin?
	 */
	private boolean foundFood = false;

	public AntCell getActiveCell() {
		return activeCell;
	}

	public boolean hasFoundFood() {
		return foundFood;
	}

	public void setFoundFood(boolean found) {
		this.foundFood = found;
	}

	public void setActiveCell(AntCell activeCell) {
		if (this.activeCell != null) {
			this.activeCell.removeAnt();
		}
		this.activeCell = activeCell;
		this.activeCell.addAnt();
	}

	public List<AntAction> getActions() {
		return activeCell.getActions();
	}

	/**
	 * Calcule ici la somme du produit des phéromones et de la distance
	 * jusqu'au point final
	 * 
	 * @return
	 */
	public double pherDistanceSum() {
		List<AntAction> actions = getActions();
		double total = 0;
		for (AntAction action : actions) {
			total += Math.pow(action.getPherTrail(), MazeSolver.getAntAlpha())
					* Math.pow((1 / action.getDistanceToFinalPoint()), MazeSolver.getAntBeta());
		}
		return total;
	}

	public void setLastAction(AntAction action) {
		if (!foundFood) {
			// Supprime du chemin les parties inutiles (ex: A-B-C-D-E-B-F,
			// boucle de B � B -> le chemin devient A-B-F)
			for(AntAction previousAction : actionHistory) {
				if(previousAction.getSource()==action.getSource()) {
					int index = actionHistory.indexOf(previousAction);
					distance = index;
					actionHistory.subList(index, actionHistory.size()).clear();
					break;
				}
			}
			distance++;
			actionHistory.add(action);
		}
		this.lastAction = action;
	}

	public AntAction getLastAction() {
		return lastAction;
	}

	/**
	 * Si la fourmi est sur le retour elle suis le chemin qu'elle a pris �
	 * l'aller en sens inverse Sinon elle choisi au hasard une des actions
	 * voisines (en �vitant de revisiter la cellule qu'elle vient de visiter si
	 * possible)
	 * 
	 * @return L'action selectionn�e
	 */
	public AntAction pickAction() {
		foundFood = (foundFood || activeCell.hasMark(Cell.END)) && !actionHistory.isEmpty();
		if (actionHistory.isEmpty()) {
			distance = 0;
		}
		if (!foundFood) {
			for (AntAction action : getActions()) {
				action.computeProbability(pherDistanceSum());
			}
			List<AntAction> actions = getActions();
			double p = Math.random();
			double cumulativeProbability = 0.0;
			AntAction savedAction = null;
			for (AntAction action : actions) {
				cumulativeProbability += action.getProbability();
				if (p <= cumulativeProbability) {
					// If pour �viter de faire un aller retour sur place
					if (lastAction != null && action.destination == lastAction.getSource() && actions.size() > 1) {
						continue;
					}
					return action;
				} else {
					savedAction = action;
				}
			}
			if(savedAction==null) {
				savedAction = actions.get(0);
			}
			return savedAction;
		} else {
			// Si on est en train de retourner vers le d�part apr�s avoir trouv�
			// la fin
			return actionHistory.removeLast();
		}

	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}
}
