package com.nullprogram.maze;

public class AntAction extends Action {

	public static double MIN_TRAIL = 0.00001;
	private double probability;
	private double pherTrail = MIN_TRAIL;
	private double distanceToFinalPoint = 0;

	public AntAction(DepthCell cell, DepthCell source) {
		super(cell, source);

	}

	public void computeDistanceToExit() {
		distanceToFinalPoint = MazeSolver.computeManhattanDistance(this.destination.x, this.destination.y);
	}

	public void computeProbability(double totalSum) {

		double firstPart = Math.pow(pherTrail, MazeSolver.getAntAlpha());
		double secondPart = Math.pow((1 / distanceToFinalPoint), MazeSolver.getAntBeta());
		if (Double.isNaN(totalSum) || Double.isNaN((firstPart * secondPart) / totalSum)) {
			probability = 0; // bug corrig�! on ne devrait plus avoir besoin de
								// cette partie mais je garde quand meme
		} else {
			probability = (firstPart * secondPart) / totalSum;
		}

	}

	public void updatePherTrail(double pherAmount) {
		pherTrail = Math.max(pherTrail * (1 - MazeSolver.getAntRho()) + pherAmount, MIN_TRAIL);
	}

	public double getPherTrail() {
		return pherTrail;
	}

	public double getDistanceToFinalPoint() {
		return distanceToFinalPoint;
	}

	public double getProbability() {
		return probability;
	}

	public void setPherTrail(double trail) {
		this.pherTrail = trail;

	}
	
	public void print() {
		System.out.println(source+" "+destination);
	}

}
