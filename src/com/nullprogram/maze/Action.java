package com.nullprogram.maze;

public class Action {
	protected DepthCell destination;
	protected DepthCell source;

	public Action(DepthCell cell, DepthCell source) {
		this.destination = cell;
		this.source = source;
	}

	public DepthCell getCell() {
		return destination;
	}

	public DepthCell getSource() {
		return source;
	}
}