package com.nullprogram.maze;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.text.DecimalFormat;

/**
 * Displays a maze to the user as a GUI component.
 */
class MazeDisplay extends JPanel implements SolverListener {
	private static final long serialVersionUID = 1L;

	private static final Stroke WALL_STROKE = new BasicStroke(2);
	private static final Stroke ARROW_STROKE = new BasicStroke(1);
	/* Color scheme. */
	private static final Color SOLUTION_COLOR = Color.GREEN;
	private static final Color ERROR_COLOR = new Color(255, 127, 127);
	private static final Color WALL_COLOR = Color.YELLOW;
	private static final Color ARROW_COLOR = Color.BLUE;
	private static final Color V_COLOR = Color.RED;
	private static final Color END_COLOR = Color.ORANGE;
	private Maze maze;
	private double endR = 10000;

	/**
	 * Display the given maze at the given size.
	 * 
	 * @param view
	 *            the maze to be displayed
	 */
	public MazeDisplay(final Maze view) {
		super();
		setMaze(view);
	}

	public Maze getMaze() {
		return maze;
	}

	@Override
	public void paintComponent(final Graphics graphics) {
		super.paintComponent(graphics);
		Graphics2D g = (Graphics2D) graphics;
		double scaleX = getWidth() * 1.0 / maze.getWidth();
		double scaleY = getHeight() * 1.0 / maze.getHeight();
		g.scale(scaleX, scaleY);
		g.setStroke(WALL_STROKE);
		double minV = Double.MAX_VALUE;
		double maxV = endR;
		double maxPher = 0;
		int antCount = 0;
		for (Cell cell : maze) {
			if (cell.getClass() == QCell.class) {
				QCell qcell = (QCell) cell;
				minV = Math.min(minV, qcell.getMaxQ());
				maxV = Math.max(maxV, qcell.getMaxQ());
			} else if (cell.getClass() == AntCell.class) {
				AntCell antCell = (AntCell) cell;
				maxPher = Math.max(maxPher, antCell.getMaxPher());
				antCount += antCell.getAntCount();
			}
		}
		for (Cell cell : maze) {
			if (cell.hasMark(MazeSolver.ERROR_MARK)) {
				g.setColor(ERROR_COLOR);
				g.fill(cell.getShape());
			} else if (cell.hasMark(Cell.END)) {
				g.setColor(END_COLOR);
				g.fill(cell.getShape());
			} else if (cell.getClass() == QCell.class) {

				QCell qcell = (QCell) cell;
				/**
				 * Genere une couleur de base à partir de la valeur V
				 */
				int baseColor = Math.min(Math.max((int) (255 * (qcell.getMaxQ() - minV) / (maxV - minV)), 0), 255);
				/**
				 * Si le chemin fait partie de la solution on le colore plus
				 * clairement
				 */
				int baseGreen = 0;
				if (cell.hasMark(MazeSolver.SOLUTION_MARK)) {
					baseGreen = Math.max(0, Math.min(baseColor + 100, 255));
				}

				int baseRed = baseColor;
				int baseBlue = baseColor;
				

				g.setColor(new Color(baseGreen, baseRed, baseBlue));
				g.fill(cell.getShape());

				if(cell.hasMark(MazeSolver.EXPLORING_MARK)) {
					g.setColor(Color.GREEN);
					g.fill(qcell.getTriangle());
					g.setStroke(ARROW_STROKE);
					g.setColor(new Color(255, 255, 255));
					g.draw(qcell.getTriangle());
				}
			} 

			if (cell.getClass() == QCell.class) {

				QCell qcell = (QCell) cell;
				/**
				 * Dessine le trait indiquant le chemin optimal
				 */
				g.setColor(ARROW_COLOR);
				g.setStroke(ARROW_STROKE);
				Shape arrow = qcell.getOptimalArrow();
				if (arrow != null) {
					g.fill(arrow);
				}

				/**
				 * Dessine la valeur de V pour cette cellule
				 */
				g.setColor(V_COLOR);
				g.setFont(new Font("TimesRoman", Font.PLAIN, (int) (.38 * qcell.getSize())));
				double v = qcell.getMaxQ();
			//	v = Math.round(v);
				g.drawString(format(v)+"", (int) (qcell.getCenterX() - qcell.getSize()/2.5), (int) qcell.getCenterY());

			} else if (cell.getClass() == AntCell.class) {
				AntCell antCell = (AntCell) cell;

				int baseColor = maxPher > AntAction.MIN_TRAIL ? (int) (255 * (antCell.getMaxPher()) / (maxPher)) : 0;
				baseColor = Math.min(255, baseColor);
				if (cell.hasMark(Cell.END)) {
					g.setColor(END_COLOR);
					} else {
				g.setColor(new Color(baseColor, baseColor, baseColor));
					}
				g.fill(cell.getShape());
				 

				// Indicateur de pr�sence de fourmis
				if (antCell.getAntCount() > 0) {
					int baseRed = Math.min(255, 255 * antCell.getAntCount() / antCount);
					g.setColor(new Color(baseRed, baseRed, baseRed));
					g.fill(antCell.getEllipse());
					g.setStroke(ARROW_STROKE);
					g.setColor(new Color(255, 255, 255));
					g.draw(antCell.getEllipse());
				}

				g.setColor(V_COLOR);
				g.setFont(new Font("TimesRoman", Font.PLAIN, (int) (.38 * antCell.getSize())));

				double v = antCell.getMaxPher();
				
				g.drawString(format(v), (int) (antCell.getCenterX() - antCell.getSize()/2.5), (int) antCell.getCenterY());
				
			}

			g.setStroke(WALL_STROKE);
			g.setColor(WALL_COLOR);
			g.draw(cell.getWalls());
		}
	}

	/**
	 * Assign a new maze to this display.
	 * 
	 * @param view
	 *            the new maze to be displayed
	 */
	public void setMaze(final Maze view) {
		maze = view;
		Dimension size = new Dimension(maze.getWidth(), maze.getHeight());
		setMinimumSize(size);
		setPreferredSize(size);
		repaint();
	}

	@Override
	public final void solveDone() {
		repaint();
	}

	@Override
	public final void solveStep() {
		repaint();
	}

	public void setEndReward(Double endReward) {
		this.endR = endReward;

	}
	
	private static String[] suffix = new String[]{"","k", "M", "b", "t"};
	private static int MAX_LENGTH = 5;

	private static String format(double number) {

        Measurement dist = new Measurement(number,"");
	    return dist.toString();
	}
}
