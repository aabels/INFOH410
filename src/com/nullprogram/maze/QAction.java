package com.nullprogram.maze;

public class QAction extends Action {

	public final static double DEFAULT_Q = 0;//0.01;
	private double q = DEFAULT_Q;

	public QAction(DepthCell cell, DepthCell source) {
		super(cell, source);
		// TODO Auto-generated constructor stub
	}

	public double getReward() {
		return ((QCell) destination).getR();
	}

	public double getVValue() {
		return ((QCell) destination).getMaxQ();
	}

	public QCell getQCell() {
		return (QCell) getCell();
	}

	public void setQValue(double q) {
		this.q = q;
	}

	public double getQValue() {
		return q;
	}

	public void clearQ() {
		q = DEFAULT_Q;
	}

}
