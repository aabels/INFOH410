package com.nullprogram.maze;

import java.awt.*;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;

public class QCell extends DepthCell {
	
	private double explorationFactor = 1;

	
	public QCell(int posX, int posY, int scale) {
		super(posX, posY, scale);
		// TODO Auto-generated constructor stub
	}

	private List<QAction> actions = new ArrayList<QAction>();
	private double r = 10;

	/**
	 * Genere un trait dans la direction de l'action optimale
	 * 
	 * @return le trait generé
	 */
	public final Shape getOptimalArrow() {
		QAction optimalAction = getOptimalAction();
		if(hasMark(Cell.END)) {
			return null;
		}
		if (optimalAction != null && optimalAction.getCell() != null) {
			Shape arrow = new Line2D.Double(getCenterX(), getCenterY(),
					getCenterX() + (optimalAction.getQCell().getCenterX() - getCenterX()) / 2,
					getCenterY() + (optimalAction.getQCell().getCenterY() - getCenterY()) / 2);
			int startX = (int) getCenterX();
			int endX = (int) (getCenterX() + (optimalAction.getQCell().getCenterX() - getCenterX()) / 2);
			int startY = (int) getCenterY();
			int endY = (int) (getCenterY() + (optimalAction.getQCell().getCenterY() - getCenterY()) / 2);
			 int xPoly[] = {startX,endX,startX+(endX-startX)/2,startX+(endX-startX)/2,endX};
			 
		     int yPoly[] = {startY,endY,startY+(endY-startY)/2,startY+(endY-startY)/2,endY};

		     if(startX!=endX) {
		    	 yPoly[2]+=size/6;
		    	 yPoly[3]-=size/6;
		     }
		     if(startY!=endY) {

		    	 xPoly[2]+=size/6;
		    	 xPoly[3]-=size/6;
		     }
			Shape polygon = new Polygon(xPoly,yPoly,xPoly.length);
			
			return polygon;
		}
		return null;
	}

	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
	}

	/**
	 * Add a connecting cell to this cell.
	 * 
	 * @param cell
	 *            a new cell connection
	 */
	@Override
	public void addNeighbor(final DepthCell cell) {
		QAction action = new QAction(cell, this);
		actions.add(action);
		if (cell.x < x) {
			left = false;
		} else if (cell.y < y) {
			top = false;
		}
		if (!neighbors.contains(cell)) {
			neighbors.add(cell);
		}
	}

	@Override
	public QAction getOptimalAction() {
		QAction optimal = null;
		for (QAction action : actions) {
			if (optimal == null || action.getQValue() > optimal.getQValue()) {
				optimal = action;
			}
		}
		return optimal;
	}

	public double getMaxQ() {
		QAction optimal = getOptimalAction();
		return optimal != null ? optimal.getQValue() : 0;
	}

	public void clearActionQValues() {
		for (QAction action : actions) {
			action.clearQ();
		}
	}

	@Override
	public void removeNeighbor(DepthCell cell) {
		if(this.neighbors.contains(cell)) {
			this.neighbors.remove(cell);
			if (cell.x < x) {
				left = true;
			} else if (cell.y < y) {
				top = true;
			}

			List<Action> toRemove = new ArrayList<Action>();
			for (Action action : actions) {
				if (action.destination == cell) {
					toRemove.add(action);
				}
			}
			actions.removeAll(toRemove);

		}

	}

	public List<QAction> getActions() {
		return actions;
	}

	public QAction pickNext() {
		double qSum = 0;
		double min = Double.MAX_VALUE;

		for(QAction action : actions) {
			qSum += Math.pow(explorationFactor, action.getQValue());
		}
		
		double p = Math.random();
		double cumulativeProbability = 0.0;
		for (QAction action : actions) {
			cumulativeProbability += Math.pow(explorationFactor, action.getQValue())/qSum;
			
			if (p <= cumulativeProbability ) {
				return action;
			}
		}
		return null;
	}

	public void setExplorationFactor(Double explorationFactor) {
		this.explorationFactor = explorationFactor;
	}
}
