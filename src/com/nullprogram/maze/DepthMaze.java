package com.nullprogram.maze;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.Queue;

/**
 * A single randomly-generated maze. The maze is generated at construction time.
 */
public class DepthMaze implements Maze {

	private int method;
	private static final int NDIRECTIONS = 4;

	private static final Mark VISITED = Mark.get("visited");

	private final int width;
	private final int height;
	private final int scale;
	private final DepthCell[][] data;
	private double endR = 1000;
	private int complexityPct = 100;
	private double defaultR = 10;
	private Double explorationFactor = QAction.DEFAULT_Q;

	/**
	 * Generate a new maze with the given properties.
	 * 
	 * @param cellWidth
	 *            width of the new maze
	 * @param cellHeight
	 *            height of the new maze
	 * @param cellScale
	 *            scale of this maze
	 */
	public DepthMaze(final int cellWidth, final int cellHeight, final int cellScale, final int complexity) {
		this(cellWidth, cellHeight, cellScale, complexity, 1000., 10., -1);
	}

	public DepthMaze(int mazeWidth, int mazeHeight, int mazeScale, int complexity, Double endR, Double defaultR, int method) {
		this.method = method;
		this.endR = endR;
		this.defaultR = defaultR;
		width = mazeWidth;
		height = mazeHeight;
		scale = mazeScale;
		data = new DepthCell[width][height];
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				switch (method) {
				case MazeSolver.METHOD_ANT:

					data[i][j] = new AntCell(i, j, scale);
					break;
				case MazeSolver.METHOD_Q:

					data[i][j] = new QCell(i, j, scale);
					break;
				default:

					data[i][j] = new DepthCell(i, j, scale);

				}
			}
		}
		complexityPct = complexity;
		generate();
	}

	public void applyDefaultR(double r) {
		for (DepthCell[] column : data) {
			for (DepthCell cell : column) {
				((QCell) cell).setR(r);

			}
		}
	}

	public void applyEndR(double r) {
		((QCell) data[width - 1][height - 1]).setR(r);
	}

	/**
	 * Generate this maze. This should only be called once.
	 */
	private void generate() {
		Deque<Point> stack = new ArrayDeque<Point>();
		stack.push(new Point(0, 0));
		data[0][0].mark(VISITED);
		data[width - 1][height - 1].mark(Cell.END);
		if (this.method == MazeSolver.METHOD_Q) {
			applyEndR(endR);
		}
		while (!stack.isEmpty()) {
			Queue<Point> dirs = getDirections();
			Point p = stack.peek();
			DepthCell current = get(p);
			boolean found = false;
			while (!dirs.isEmpty()) {
				Point dir = dirs.poll();
				Point pnext = new Point(dir.x + p.x, dir.y + p.y);
				DepthCell next = get(pnext);

				/**
				 * Ajoute (avec une probabilité de 100-COMPLEXITY_PCT) la
				 * cellule à la liste des voisins, c'est à dire les cellules
				 * voisines pour lesquels il n'y a pas de mur. On peut ainsi
				 * jouer sur la complexité du labyrinthe
				 */
				if (next != null) {
					int prob = (int) (Math.random() * 100);
					if (prob < 100 - complexityPct) {
						next.addNeighbor(current);
						current.addNeighbor(next);
					}
				}

				if (next == null || next.hasMark(VISITED)) {
					continue;
				}

				/**
				 * Construction de chemin normal, les lignes ci-dessous
				 * permettent d'assurer qu'il y a au moins un chemin du début
				 * à la fin
				 */
				next.addNeighbor(current);
				current.addNeighbor(next);

				stack.push(pnext);
				next.mark(VISITED);

				found = true;

				break;
			}
			if (!found) {
				stack.pop();
			}
		}
	}

	/**
	 * A randomly-sorted set of directions to go.
	 * 
	 * @return a randomly-sorted queue of directions
	 */
	private Queue<Point> getDirections() {
		List<Point> dirs = new ArrayList<Point>(NDIRECTIONS);
		dirs.add(new Point(1, 0));
		dirs.add(new Point(-1, 0));
		dirs.add(new Point(0, 1));
		dirs.add(new Point(0, -1));
		Collections.shuffle(dirs);
		return new ArrayDeque<Point>(dirs);
	}

	/**
	 * Return the cell at the given position.
	 * 
	 * @param p
	 *            position of the cell
	 * @return the cell at the given position
	 */
	private DepthCell get(final Point p) {
		if (inBounds(p.x, p.y)) {
			return data[p.x][p.y];
		} else {
			return null;
		}
	}

	/**
	 * Return true if the given position is within the bounds of the maze.
	 * 
	 * @param px
	 *            X position to be tested
	 * @param py
	 *            Y position to be tested
	 * @return true if position is within the bounds of the maze
	 */
	private boolean inBounds(final int px, final int py) {
		return (px >= 0) && (py >= 0) && (px < width) && (py < height);
	}

	@Override
	public final int getWidth() {
		return width * scale;
	}

	@Override
	public final int getHeight() {
		return height * scale;
	}

	@Override
	public final Cell start() {
		return data[0][0];
	}

	@Override
	public final Iterator<Cell> iterator() {
		return new DepthMazeIterator();
	}

	/**
	 * Iterates through all the cells in the current maze, starting with 0, 0,
	 * working row-wise.
	 */
	private class DepthMazeIterator implements Iterator<Cell> {
		private int x = 0;
		private int y = 0;

		@Override
		public boolean hasNext() {
			return y < height;
		}

		@Override
		public Cell next() {
			if (y >= height) {
				throw new NoSuchElementException();
			}
			Cell ret = data[x][y];
			x++;
			if (x >= width) {
				x = 0;
				y++;
			}
			return ret;
		}

		@Override
		public void remove() {
			String msg = "Can't remove cells from a maze.";
			throw new UnsupportedOperationException(msg);
		}
	}

	/**
	 * Supprime cette marque de toutes les cellules
	 * 
	 * @param mark
	 */
	public void cleanMarks(Mark mark) {
		for (DepthCell[] column : data) {
			for (DepthCell cell : column) {
				cell.removeMark(mark);
			}
		}
	}

	
	public int getMazeHeight() {
		return this.height;
	}

	public int getMazeWidth() {
		return this.width;
	}

	void convert(int method) {
		if (method != this.method) {
			this.method = method;
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					DepthCell current = data[i][j];
					switch (method) {
					case MazeSolver.METHOD_ANT:
						data[i][j] = new AntCell(current.x, current.y, scale);
						break;
					case MazeSolver.METHOD_Q:
						data[i][j] = new QCell(current.x, current.y, scale);
						break;
					default:
						data[i][j] = new DepthCell(current.x, current.y, scale);
					}
					if(current.hasMark(Cell.END)) {
						data[i][j].mark(Cell.END);
					}
					for (Cell cell : current.neighbors()) {
						DepthCell neighbour = (DepthCell) cell;
						data[i][j].addNeighbor(data[neighbour.x][neighbour.y]);
					}
				}
			}

			for (DepthCell[] column : data) {
				for (DepthCell current : column) {
					for (Cell cell : current.neighbors()) {
						DepthCell neighbour = (DepthCell) cell;
						current.removeNeighbor(neighbour);
						current.addNeighbor(data[neighbour.x][neighbour.y]);
					}

				}
			}


		}
	}

	public void applyExplorationFactor(Double k) {
		this.explorationFactor = k;
		applyExplorationFactor();
	}
	
	public void applyExplorationFactor() {
		
		for (DepthCell[] column : data) {
			for (DepthCell cell : column) {
				((QCell) cell).setExplorationFactor(explorationFactor);
			}
		}
	}

	public void clearQValues() {
		for (DepthCell[] column : data) {
			for (DepthCell cell : column) {
				if(cell.getClass()==QCell.class) {
					((QCell)cell).clearActionQValues();
				}
			}
		}
	}
}
