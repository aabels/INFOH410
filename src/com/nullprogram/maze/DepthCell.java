package com.nullprogram.maze;

import java.util.ArrayList;
import java.util.Collection;

import java.awt.Shape;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;

/**
 * Cell implementation for a standard rectangular maze. This was particularly
 * written for a depth-first generated maze.
 */
public class DepthCell extends Cell {

	private final Shape shape;
	protected final int x;
	protected final int y;
	protected final int size;

	public int getSize() {
		return size;
	}

	protected final Collection<Cell> neighbors = new ArrayList<Cell>(4);

	protected boolean left = true;
	protected boolean top = true;

	/**
	 * Create a new cell at the given maze position at the given scale.
	 * 
	 * @param posX
	 *            maze's X position of this cell
	 * @param posY
	 *            maze's Y position of this cell
	 * @param scale
	 *            scale of the owning maze
	 */
	public DepthCell(final int posX, final int posY, final int scale) {
		x = posX;
		y = posY;
		size = scale;
		shape = new Rectangle(x * scale, y * scale, size, size);
	}

	@Override
	public final Shape getShape() {
		return shape;
	}

	@Override
	public Shape getWalls() {
		Path2D.Double walls = new Path2D.Double();
		if (left) {
			Shape leftWall = new Line2D.Double(x * size, y * size, x * size, y * size + size);
			walls.append(leftWall, false);
		}
		if (top) {
			Shape topWall = new Line2D.Double(x * size, y * size, x * size + size, y * size);
			walls.append(topWall, false);
		}
		return walls;
	}

	@Override
	public Collection<Cell> neighbors() {
		return new ArrayList<Cell>(neighbors);
	}

	/**
	 * Add a connecting cell to this cell.
	 * 
	 * @param cell
	 *            a new cell connection
	 */
	void addNeighbor(final DepthCell cell) {
		if (cell.x < x) {
			left = false;
		} else if (cell.y < y) {
			top = false;
		}
		if (!neighbors.contains(cell)) {
			neighbors.add(cell);
		}
	}

	public void removeNeighbor(DepthCell cell) {
		if(this.neighbors.contains(cell)) {
			this.neighbors.remove(cell);
		}

	}
	/**
	 * G�n�re une ellipse pour indiquer la pr�sence de fourmis dans cet �tat
	 * 
	 * @return
	 */
	public Shape getEllipse() {
		Ellipse2D el = new Ellipse2D.Double(x * size, y * size + size / 2, size / 2, size / 2);
		return el;

	}
	
	/**
	 * G�n�re une ellipse pour indiquer la pr�sence de fourmis dans cet �tat
	 * 
	 * @return
	 */
	public Shape getTriangle() {
		int xPol[] = {x*size,x*size+size/2,x*size};
		int yPol[] = {y*size+size/2,y*size+size,y*size+size};
		Polygon pol = new Polygon(xPol,yPol,xPol.length);
		return pol;

	}
	public Action getOptimalAction() {
		return null;
	}

	public double getCenterX() {
		return x * size + size / 2;
	}

	public double getCenterY() {
		return y * size + size / 2;
	}

}
