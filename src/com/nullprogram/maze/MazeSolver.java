package com.nullprogram.maze;

import java.util.Deque;
import java.util.List;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Solves a maze asynchronously at a leisurely pace.
 */
class MazeSolver implements Runnable {

	public static final Mark ERROR_MARK = Mark.get("error");
	public static final Mark SOLUTION_MARK = Mark.get("solution");
	public static final Mark EXPLORED_MARK = Mark.get("explored");
	public static final Mark EXPLORING_MARK = Mark.get("exploring");
	public static final int METHOD_ANT = 1;
	public static final int METHOD_Q = 0;
	private static Maze maze;
	private int sleepTime;
	private int method = 0;
	private boolean pause = false;
	private double discountFactor = 0.999;
	private double learningRate = 1;
	/**
	 * True if the last update changed at least one V value
	 */
	private boolean change = true;

	private volatile boolean enabled;
	private Collection<SolverListener> listeners = new CopyOnWriteArrayList<SolverListener>();
	private int antCount;
	private static double antAlpha;
	private static double antBeta;
	private static double antRho;
	private static double antQ;

	/**
	 * Creates a new solver for the given maze.
	 * 
	 * @param puzzle
	 *            the maze to be solved
	 * @param sleep
	 *            amount of time to sleep between steps
	 */
	public MazeSolver(final Maze puzzle, final int sleep) {
		maze = puzzle;
		sleepTime = sleep;
	}

	/**
	 * Start the solver thread.
	 */
	public void start(int method) {

		(new Thread(this)).start();
		this.method = method;
		enabled = true;
	}

	/**
	 * Pause the solution thread.
	 */
	public void stop() {
		enabled = false;
	}

	@Override
	public void run() {
		// depthSolve();
		switch (method) {
		case METHOD_ANT:
			antColonySolve();
			break;
		case METHOD_Q:
			qLearningSolve();
			break;

		default:
			depthSolve();
			break;
		}
	}

	private void antColonySolve() {
		ArrayList<Ant> ants = new ArrayList<Ant>();
		for (int i = 0; i < antCount; i++) {
			ants.add(new Ant());
		}
		for (Cell cell : maze) {
			AntCell antCell = (AntCell) cell;

			antCell.setAntCount(0);
			for (AntAction action : antCell.getActions()) {
				action.setPherTrail(AntAction.MIN_TRAIL);
				action.computeDistanceToExit();
			}
		}

		for (Ant ant : ants) {
			ant.setActiveCell((AntCell) maze.start());
		}

		while (enabled) {
			if(pause) {
				try {
					Thread.sleep(100);
				} catch (Exception ex) {
				}
				continue;
			}
			try {
				Thread.sleep(sleepTime);
			} catch (Exception ex) {
			}

			if (!enabled) {
				continue;
			}
			generateAndSelectSolutions(ants);
			pheromoneUpdate(ants);
			for (SolverListener listener : listeners) {
				listener.solveStep();
			}

		}

		for (SolverListener listener : listeners) {
			listener.solveDone();
		}

	}

	/**
	 * Mettre a jour les phéromone à chaque fois qu'on y passe
	 */
	private void pheromoneUpdate(List<Ant> ants) {
		for (Cell cell : maze) {

			for (AntAction action : ((AntCell) cell).getActions()) {
				double pherAmount = 0;
				for (Ant ant : ants) {

					if (action.equals(ant.getLastAction()) && ant.hasFoundFood()) {
						pherAmount += antQ / ant.getDistance();
					}
				}
				action.updatePherTrail(pherAmount);
			}
		}
		;

	}

	/**
	 * Calculer la probabilité des actions
	 * 
	 * @param ants
	 */
	public void generateAndSelectSolutions(List<Ant> ants) {
		for (Ant ant : ants) {

			AntAction pickedAction = ant.pickAction();
			if (ant.hasFoundFood()) {
				ant.setActiveCell((AntCell) pickedAction.getSource());
			} else {
				if (pickedAction != null) {
					ant.setActiveCell((AntCell) pickedAction.getCell());
				}
			}
			ant.setLastAction(pickedAction);
		}

	}

	/**
	 * Solve the maze using Q Learning
	 */
	private void qLearningSolve() {

		/**
		 * Remet à zéro l'apprentissage précédent
		 */
		((DepthMaze) maze).cleanMarks(MazeSolver.SOLUTION_MARK);
		((DepthMaze) maze).cleanMarks(EXPLORING_MARK);
		((DepthMaze) maze).applyExplorationFactor();
		((DepthMaze) maze).clearQValues();
		

		QCell root = (QCell) maze.start();
		change = true;
		boolean changeDetected = false;
		while (change && enabled) {
			
			if(pause) {
				try {
					Thread.sleep(100);
				} catch (Exception ex) {
				}
				continue;
			}
			try {
				Thread.sleep(sleepTime);
			} catch (Exception e) {
				return;
			}
			if (!enabled) {
				continue;
			}
			change = true;
			if(	root.hasMark(Cell.END)) {
				((DepthMaze) maze).cleanMarks(EXPLORED_MARK);

				((DepthMaze) maze).cleanMarks(EXPLORING_MARK);

				((DepthMaze) maze).cleanMarks(SOLUTION_MARK);
				root = (QCell) maze.start();
				markPathToEnd(root);
			}
			
			double startV = root.getMaxQ();

			//for (Action action : cell.getActions()) {
				QAction qaction = (QAction) root.pickNext();
				double qvalue = qaction.getQValue() + learningRate * (qaction.getReward()
						+ ((QCell) qaction.getCell()).getMaxQ() * discountFactor - qaction.getQValue());
				qaction.setQValue(qvalue);

				changeDetected = changeDetected || root.getMaxQ() != startV;
				root.removeMark(EXPLORING_MARK);
				root =  (QCell)qaction.getCell();
				root.mark(EXPLORING_MARK);
			//}

			// La mise à jour s'arretera quand les V ont atteint un état
			// stable

			for (SolverListener listener : listeners) {
				listener.solveStep();
			}

			

		}

		markPathToEnd(root);
		for (SolverListener listener : listeners) {
			listener.solveDone();
		}
	}

	/**
	 * Follows the optimal actions and marks all cells along the path
	 * 
	 * @param cell
	 *            start of the path
	 */
	private void markPathToEnd(DepthCell cell) {
		if (cell.hasMark(SOLUTION_MARK) || cell.hasMark(Cell.END)) {
			// Boucle
			return;
		}
		cell.mark(SOLUTION_MARK);

		if (cell.getOptimalAction() != null) {
			markPathToEnd(cell.getOptimalAction().getCell());
		}
	}

	/**
	 * Updates the cell's V value then visits all neighbours to update their V
	 * values
	 * 
	 * @param cell
	 *            the cell to be visited
	 * @return the v value of the visited cell
	 */
	private double updateV(QCell cell) {

		while (/*!cell.hasMark(EXPLORED_MARK) &&*/ !cell.hasMark(Cell.END)) {
			//cell.mark(EXPLORED_MARK);

			

		}

		return cell.getMaxQ();
	}

	private void depthSolve() {
		Deque<Cell> stack = new ArrayDeque<Cell>();
		stack.push(maze.start());
		stack.peek().mark(SOLUTION_MARK);

		while (!stack.peek().hasMark(Cell.END)) {
			try {
				Thread.sleep(sleepTime);
			} catch (Exception e) {
				return;
			}
			if (!enabled) {
				continue;
			}

			Cell current = stack.peek();
			Collection<Cell> dirs = current.neighbors();

			boolean found = false;
			for (Cell next : dirs) {
				if (next.hasMark(ERROR_MARK) || next.hasMark(SOLUTION_MARK)) {
					continue;
				}
				stack.push(next);
				next.mark(SOLUTION_MARK);
				found = true;
				break;
			}
			if (!found) {
				stack.pop().mark(ERROR_MARK);
			}
			for (SolverListener listener : listeners) {
				listener.solveStep();
			}
		}
		for (SolverListener listener : listeners) {
			listener.solveDone();
		}
	}

	/**
	 * Subscribe a new SolverListener to this solver.
	 * 
	 * @param listener
	 *            the new subscriber
	 */
	public final void addListener(final SolverListener listener) {
		listeners.add(listener);
	}

	public void setDiscountFactor(double discountFactor) {
		this.discountFactor = discountFactor;
	}

	public void setLearningRate(double learningRate) {
		this.learningRate = learningRate;
	}

	public void setAntCount(int antCount) {
		this.antCount = antCount;
	}

	public void setAntAlpha(Double alpha) {
		this.antAlpha = alpha;
	}

	public void setAntBeta(Double beta) {
		this.antBeta = beta;
	}

	public static double getAntAlpha() {
		return antAlpha;
	}

	public static double getAntBeta() {
		return antBeta;
	}

	public static int getMazeWidth() {
		return maze.getWidth();
	}

	public static int getMazeHeight() {
		return maze.getHeight();
	}

	public static double computeManhattanDistance(int width, int height) {
		double dist = maze.getMazeHeight() - (1 + height) + maze.getMazeWidth() - (1 + width);
		if (dist == 0) {
			dist = 0.001;// rajouté pour le cas ou on se trouve sur l'arrivée!
							// si on laisse on zero on aura une divison par zero
		}

		return dist;

	}

	public static void setAntRho(double rho) {
		antRho = rho;
	}

	public static double getAntRho() {
		return antRho;
	}

	public static void setAntQ(double q) {
		antQ = q;
	}

	public static double getAntQ() {
		return antQ;
	}

	public void pause() {
		this.pause = true;
	}

	public void play() {
		this.pause = false;

	}

	public void setSleepTime(int sleepValue) {
		this.sleepTime = sleepValue;
		
	}
}
